#!/usr/bin/env node

const { eraseLines } = require('ansi-escapes');
const wrapAnsi = require('wrap-ansi');

module.exports = function smoothWriter(stream, interval = 500) {
  let queue = [];
  let count = 0;

  let id = setInterval(flush, interval);

  function flush() {
    if (queue.length > 0) {
      if (count > 0) {
        stream.write(eraseLines(count));
      }

      const info = queue.join('\n') + '\n';
      count = info.split('\n').length;

      queue = [];

      if (!stream.write(info)) {
        clearInterval(id);
        stream.once('drain', () => {
          id = setInterval(flush, interval)
        });
      }
    }
  }

  let changed = true;

  function nextEventLoop() {
    changed = true;
  }

  function isSameEventLoop() {
    if (!changed) {
      return true;
    }

    setImmediate(nextEventLoop);
    return changed = false;
  }

  return function write(data = '') {
    if (!isSameEventLoop()) {
      queue = [];
    }

    queue.push(data.length > stream.columns ?
      wrapAnsi(data, stream.columns) : data);
  };
}

